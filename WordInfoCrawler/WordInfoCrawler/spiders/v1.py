# -*- coding: utf-8 -*-
# (c) 2019 Shabang Systems, LLC
# Created by Houjun Liu

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import csv
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

class V1Spider(CrawlSpider):
    name = 'v1'
    start_urls = ['http://wordinfo.info/units/index/page:1']
    rules = (
        Rule(LinkExtractor(restrict_xpaths="//a[@class='next']"), follow=True),
        Rule(LinkExtractor(allow=r"\/unit\/\d*\/ip:\d*$"), callback='parseIndex'),
    )

    def parseIndex(self, response):
        wordUnitName = response.css("h1.title::text").extract_first().strip().replace("-", "").replace(",", " ").replace("+", " ").replace(";", " ")
        words = response.css("div.title::text").extract()
        wordList = []
        for word in words:
            strippedWord = word.replace("\t", "").strip()
            splittenWord = re.split(", |; ", strippedWord)
            wordList = wordList+splittenWord
        wordList = list(filter(None, wordList))
        print(wordUnitName)
        print("Parsing list named", wordUnitName)
        with open('extracted_roots.csv', 'a', newline='') as csvFile:
            csvFileWriter = csv.writer(csvFile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for word in wordList:
                csvFileWriter.writerow([wordUnitName, word])
        next_page = response.xpath("//a[@class='next']/@href").extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parseIndex)

